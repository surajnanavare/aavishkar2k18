<?php
include '../db.php';
$d=new DB();
$id=$_REQUEST['id'];
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='http://fonts.googleapis.com/css?family=Vollkorn|Open+Sans:400,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
	<link rel="stylesheet" href="../materialize/materialize.min.css">
	<script src="js/modernizr.js"></script> <!-- Modernizr -->
  	
	<title>3D Folding Panel</title>
</head>
<body>
	<main class="cd-main">
		<nav>
    <div class="nav-wrapper">
      <div class="col s12" style="padding-left: 10px;">
        <a href="http://aavishkargcek.com/m" class="breadcrumb">Home</a>
        <a href="#!" class="breadcrumb"><?php echo $d->get_('dept','dept_name','dept_id',$id)[0]['dept_name']; ?></a>
        
      </div>
    </div>
  </nav>
		<ul class="cd-gallery">
		

			<?php
								$event_ids=$d->get_('dept','event_id','dept_id',$id);
								$event_name=$d->get_('dept','dept_name','dept_id',$id);
								for($i=0;$i<count($event_ids);$i++){
									
								
									?>


			<li class="cd-item">
			<?php $eid=$event_ids[$i]['event_id'] ?>
				<a href="<?php echo 'item-1.php?id='.$eid ?>"  >
					<div>
						<h2><?php print_r($d->get_('basic_details','event_name','eid',$eid)[0]['event_name']); ?></h2>
						
						<b>View More</b>
					</div>
				</a>
			</li>
			<?php } ?>
			
		</ul> <!-- .cd-gallery -->
	</main> <!-- .cd-main -->

	<div class="cd-folding-panel">
		
		<div class="fold-left"></div> <!-- this is the left fold -->
		
		<div class="fold-right"></div> <!-- this is the right fold -->
		
		<div class="cd-fold-content">
			<!-- content will be loaded using javascript -->
		</div>

		<a class="cd-close" href="#0"></a>
	</div> <!-- .cd-folding-panel -->

<script src="js/jquery-2.1.1.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
<script src="../materialize/materialize.min.js"></script>
</body>
</html>