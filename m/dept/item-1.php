<?php
include '../db.php';
$d=new DB();
$eid=$_REQUEST['id'];
$fname=$d->get_('team','fname','eid',$eid);
$lname=$d->get_('team','lname','eid',$eid);
//print_r($lname);

?>

<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/reset.css"> 
	<link rel="stylesheet" href="css/style.css"> 
	<link rel="stylesheet" href="../materialize/materialize.min.css">
	<script src="js/modernizr.js"></script> 
  	
	<title>3D Folding Panel</title>
</head>
<body>
	<div class="cd-fold-content single-page">
	<?php 

			$event_title=$d->get_('basic_details','event_name','eid',$eid)[0]['event_name'];
			$event_slogan=$d->get_('basic_details','tagline','eid',$eid)[0]['tagline'];
			$day=$d->get_('basic_details','day','eid',$eid)[0]['day'];
			if($day==1) $day='21 Feb 2018';
			else $day='22 Feb 2018';
			$rules=$d->get_('rules','rule1','eid',$eid)[0]['rule1'];;
			$rounds=$d->get_('round','round1','eid',$eid)[0]['round1'];
			$descr=$d->get_('basic_details','description','eid',$eid)[0]['description'];


	 ?>


		<h2><?php print_r($event_title); ?> <span class="new badge" style="font-size: 20px;min-height: 25px;padding: 5px" data-badge-caption="<?php echo $day; ?>"></span></h2>
		<em style="font-size: 1em; font-weight: 600;margin-top: 0px;margin-bottom: 20px;"><?php echo $event_slogan; ?></em>
					<div class="card-panel teal" style="font-family: serif;text-align: justify;line-height: 30px;">
			          <span class="white-text">
			          <?php echo $descr; ?>
			          </span>
			        </div>
					<div class="card">
						    
						    <div class="card-content">
						      <span class="card-title activator grey-text text-darken-4">Rounds</span>
						      <p></p>
						      <p style="font-size: 1em;font-weight: 600;margin-top: 0px;margin-bottom: 20px;"><?php echo $rounds; ?></p>
						    </div>
						    
					</div>
					<div class="card">
						    
						    <div class="card-content">
						      <span class="card-title activator grey-text text-darken-4">Rules</span>
						      <p><?php echo $rules; ?></p>
						    </div>
						    
					</div>
					<div class="card">
						    
						    <div class="card-content">
						      <span class="card-title activator grey-text text-darken-4">Register </span>
						     
						     <a class="waves-effect waves-light btn-large" href="#">Register</a>
						    </div>
						    
					</div>
					<div class='cd-fold-text'>
						
					</div>
					<h2>Team</h2>
					<div class="row">
							<?php
								$fname=$d->get_('team','fname','eid',$eid);
								//print_r($fname);
								$lname=$d->get_('team','lname','eid',$eid);
								for($i=0;$i<count($fname);$i++){
									
								
									?>
							<div class="col s6">
								<div class="card">
								    <div class="card-image waves-effect waves-block waves-light">
								      <img class="activator" src="<?php echo $pic; ?>">
								    </div>
								    <div class="card-content">
								      <span class="card-title activator grey-text text-darken-4"><?php echo $fname[$i]['fname']." ".$lname[$i]['lname']; ?>
								      <p><a href="#">This is a link</a></p>
								    </div>
								    <div class="card-reveal">
								      <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
								      <p>Here is some more information about this product that is only revealed once clicked on.</p>
								    </div>
						  		</div>
							</div>
							<?php } ?>
					</div>

		
	</div>
</body>
<script src="js/jquery-2.1.1.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
<script src="../materialize/materialize.min.js"></script>
</body>
</html>