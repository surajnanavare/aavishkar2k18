<!DOCTYPE html>
<html lang="en-US">
<head>
	<title>Aavishkar | Add Events </title>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html">
	<meta name="author" content="Suraj S. Nanavare">
	<link href="https://fonts.googleapis.com/css?family=Laila" rel="stylesheet">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="css/font-awesome.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
	<link rel="stylesheet" type="text/css" media="all" href="css/switchery.min.css">
	<script src="https://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>
	<script src="js/home.js" type="text/javascript"></script>
	<script src="js/tinymce/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea#round1',menubar:false,
    statusbar: false });
	  tinymce.init({ selector:'textarea#rule1',menubar:false,
    statusbar: false });
	</script>
	<script type="text/javascript" src="js/switchery.min.js"></script>
	<link rel="stylesheet" href="style.css" type="text/css" />
	
	
	<script src="js/bootstrap.js" type="text/javascript"></script>
	<script type="text/javascript" charset="utf-8" language="javascript" src="js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8" language="javascript" src="js/DT_bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery_1.5.2.js"></script>
	<script type="text/javascript" src="js/vpb_uploader.js"></script>
	
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.form.js"></script>

	<style>
		.blink_me {
		animation: blinker 1s linear infinite;
		}

		@keyframes blinker {  
		  50% { opacity: .5; }
		}
	</style>
	<script type="text/javascript" >
	 $(document).ready(function() { 
			
				$('#photoimg').live('change', function()			{ 
						   $("#preview").html('');
					$("#preview").html('<img src="loader.gif" alt="Uploading...."/>');
				$("#imageform").ajaxForm({
							target: '#preview'
			}).submit();
			
				});
			}); 
	</script>

	<script type="text/javascript">
$(document).ready(function()
{
	
	// Call the main function
	new vpb_multiple_file_uploader
	({
		vpb_form_id: "form_id", // Form ID
		autoSubmit: true,
		vpb_server_url: "upload.php" 
	
	});
});
		
		
function open1(obj){
	var id=obj.id;
	var div=document.getElementsByName("option");
	for(var i=1;i<=div.length;i++)
	{
		if(i!=id)
		{
			document.getElementById("d"+i).style.display="none";
			document.getElementById(i).style.backgroundColor="";
		}
		else
		{
			document.getElementById("d"+i).style.display="";
			document.getElementById(i).style.backgroundColor="#2498e3";			
		}
	}
}

function detailed(){
$("#save_btn").html("Saving...")
	var r=tinymce.get('round1').getContent(),e=tinymce.get('rule1').getContent();$.ajax({url:"add_detailed_info.php",type:"post",data:{round1:r,round2:"",round3:"",round4:"",round5:"",rule1:e,rule2:"",rule3:"",rule5:"",rule4:"",max_team:$('#max_team').val(),note:$('#note').val()},dataType:"text",success:function(r){$("#save_btn").html("Saved")},error:function(){alert("Failed to save!");$("#save_btn").html("Retry")}})}
</script>
<style>
.preview
{
width:200px;
border:solid 1px #dedede;
padding:10px;
}
.pre
{
width:150px;
border:solid 1px #dedede;
padding:10px;
}

#preview
{
color:#cc0000;
font-size:12px
}
</style>
</head>
<body>
<?php
session_start();
include 'connect.php';
if($_SESSION['event']!="")
{	
	$ename="";
	$name="";
	$logo="";
	$logo="http://aavishkargcek.com/2k18/logos/".$_SESSION['event'].".jpg";
	$sql= "SELECT * from info WHERE eid=".$_SESSION['event'];
	$result = $conn->query($sql);
			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
					$name=$row["name"];
					$_SESSION['name']=$name;
					$ename=$row["event_name"];
				}
			}

	$stauts_for_editing=1;	
	$sql= "SELECT * from login WHERE eid=".$_SESSION['event'];
	$result = $conn->query($sql);
			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
					$stauts_for_editing=$row["times_login"];
					}
			}

			
	$tagline="";
	$non_tech="";
	$day="";
	$dept="";
	$reg_fees="";
	$cpname="";
	$cpmob="";
	$maxp="";
	$note="";
	$round1="";
	$round2="";
	$round3="";
	$round4="";
	$round5="";
	$rule1="";
	$rule2="";
	$rule3="";
	$rule4="";
	$rule5="";
	$desc="";
	$sql= "SELECT * from basic_details WHERE eid=".$_SESSION['event'];
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$tagline=$row['tagline'];
			$non_tech=$row['non_tech'];
			$day=$row['day'];
			$dept=$row['dept'];
			$reg_fees=$row['reg_fees'];
			$cpname=$row['cperson_name'];
			$cpmob=$row['cperson_mob'];
			$desc=$row['description'];
		}
	}

	$sql= "SELECT * from detailed_design WHERE eid=".$_SESSION['event'];
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$maxp=$row['max_participants'];
			$note=$row['note'];
		}
	}

	$sql= "SELECT * from round WHERE eid=".$_SESSION['event'];
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$round1=$row['round1'];
			$round2=$row['round2'];
			$round3=$row['round3'];
			$round4=$row['round4'];
			$round5=$row['round5'];
		}
	}

	$sql= "SELECT * from rules WHERE eid=".$_SESSION['event'];
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$rule1=$row['rule1'];
			$rule2=$row['rule2'];
			$rule3=$row['rule3'];
			$rule4=$row['rule4'];	
			$rule5=$row['rule5'];	
	}
	}
?>
<div class="w3-top" style="z-index:999;">
  <div class="w3-bar w3-text-white w3-card-2" style="background-color:#222a32;">
    <a href="" class="w3-bar-item w3-button" style="font-family: 'Laila', serif;"><b style="font-size:25px;">Aavishkar2k18 | Events</b></a>
	<p style="font-family: 'Laila', serif;" class="w3-right w3-container"><b><a href="logout.php" class="w3-btn w3-red w3-round w3-small"/>LOGOUT</a>&nbsp;&nbsp;&nbsp;Logged in as : <?php echo $name;?>&nbsp;&nbsp;&nbsp;&nbsp; </b></p>
  </div>
</div>
<div class="w3-sidebar  w3-bar-block w3-text-white" style="width:17%;padding-top:100px;background-color:#38444f;">
	<center>
		<img src="<?php echo $logo;?>" class="w3-white w3-border w3-border-white " alt="No Logo Uploaded Yet!" width="100px" height="100px" style="border-radius:15px;">
	</center>
	<p class=""><h4 style="font-family: 'Laila', serif;" class="w3-center w3-padding-8" ><Strong><?php echo $ename;?></Strong></h4></p>
	<p class="w3-center"><a href="http://aavishkargcek.com/events_test.php?event=<?php echo $_SESSION['event'];?>" target="_blank"><i>View Webpage</i></a></p>
	<br>
	<!--<p class=""><h6 style="font-familjky: 'Laila', serif;" class="w3-center w3-padding-8" ><Strong>Total Entries: 90</Strong></h6></p>-->
<?php 

if($stauts_for_editing)
{
?>
	<div><div onclick="javascript:open1(this);" id="1" name="option" class="w3-bar-item w3-button" style="border-bottom: 1px solid #495967;;line-height:30px;"><i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;Basic Details</div></div>
	
	<div><div onclick="javascript:open1(this);" id="2" name="option" class="w3-bar-item w3-button" style="border-bottom: 1px solid #495967;line-height:30px;"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;Detailed Infomation</div></div>
  
	<div><div onclick="javascript:open1(this);" id="4" name="option" class="w3-bar-item w3-button" style="border-bottom: 1px solid #495967;line-height:30px;"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;&nbsp;The Team</div></div>

	<div><div onclick="javascript:open1(this);" id="3" name="option" class="w3-bar-item w3-button" style="border-bottom: 1px solid #495967;line-height:30px;"><i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp;&nbsp;Logo and Banner</div></div>

	<div><a href="team" target="_blank" style="text-decoration: none;"> <div class="w3-bar-item w3-button" style="border-bottom: 1px solid #495967;line-height:30px;"><i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp;&nbsp;Team Pics</div></a></div>

	<div><div onclick="javascript:open1(this);" id="5" name="option" class="w3-bar-item w3-button" style="border-bottom: 1px solid #495967;background-color: #2498e3;line-height:30px;"><i class="fa fa-book" aria-hidden="true"></i>&nbsp;&nbsp;Instructions</div></div>

<?php
}
else
{
?>
	<div><div onclick="javascript:alert('This section has been Closed for Editing! Please Contact Administrator');" id="1" name="option" class="w3-bar-item w3-button" style="border-bottom: 1px solid #495967;;line-height:30px;"><i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;Basic Details</div></div>
	
	<div><div onclick="javascript:alert('This section has been Closed for Editing! Please Contact Administrator');" id="2" name="option" class="w3-bar-item w3-button" style="border-bottom: 1px solid #495967;line-height:30px;"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;Detailed Infomation</div></div>
  
	<div><div onclick="javascript:alert('This section has been Closed for Editing! Please Contact Administrator');" id="4" name="option" class="w3-bar-item w3-button" style="border-bottom: 1px solid #495967;line-height:30px;"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;&nbsp;The Team</div></div>

	<div><div onclick="javascript:open1(this);" id="3" name="option" class="w3-bar-item w3-button" style="border-bottom: 1px solid #495967;line-height:30px;"><i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp;&nbsp;Logo and Banner</div></div>

	<div><a href="team" target="_blank" style="text-decoration: none;"> <div class="w3-bar-item w3-button" style="border-bottom: 1px solid #495967;line-height:30px;"><i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp;&nbsp;Team Pics</div></a></div>

	<div><div onclick="javascript:open1(this);" id="5" name="option" class="w3-bar-item w3-button" style="border-bottom: 1px solid #495967;background-color: #2498e3;line-height:30px;"><i class="fa fa-book" aria-hidden="true"></i>&nbsp;&nbsp;Instructions</div></div>
<?php
}
?>
		
</div>

<div style="margin-left:17%;padding-top:100px;" class="w3-container">
	<div id="d1" style="min-height:500px;display:none">
	  <center>
	  <center id="result1"></center>
			<div class="w3-container w3-black w3-padding-8 w3-center" style="width:200px;border-radius: 32px 32px 1px 1px;"><Strong><h4>Basic Details</h4></strong></div>
	   </center>
	  <form onsubmit="return false" style="margin-top:-1px;">
	  <div class="col-2">
		<label>
		  Name
		  <input placeholder="What is event name?" id="ename" name="ename" value="<?php echo $ename;?>" tabindex="1">
		</label>
	  </div>
	  <div class="col-2">
		<label>
		  Tagline
		  <input placeholder="What is tagline of event?" id="tagline" name="tagline" tabindex="2" value="<?php  echo $tagline;?>">
		</label>
	  </div>
	 
	  <div class="col-3">
		<label>
		Is Non-technical
		<select name="non_technical" id="non_technical" tabindex="3">
			<?php
				if($non_tech=="yes")
				{
					echo '<option value="yes" selected>Yes</option>';
				}
				else
				{
					echo '<option value="yes">Yes</option>';
				}
			   
				if($non_tech=="no")
				{
					echo '<option value="no" selected>No</option>';
				}
				else
				{
					echo '<option value="no">No</option>';
				}
				
			?>
		</select>
		</label>
	  </div>
	  <div class="col-3">
		<label>
		Day of Event
		<select name="day" id="day" tabindex="4">
			<?php
				if($day=="1")
				{
					echo '<option value="1" selected>Day 1</option>';
				}
				else
				{
					echo '<option value="1">Day 1</option>';
				}
			   
				if($day=="2")
				{
					echo '<option value="2" selected>Day 2</option>';
				}
				else
				{
					echo '<option value="2">Day 2</option>';
				}
				
			?>
		</select>
		</label>
	  </div>
	  <div class="col-3">
		<label>
		  Department
		  <select name="department" id="department" tabindex="5">
			<?php
				if($dept=="Information Technology")
				{
					echo '<option value="Information Technology" selected>IT</option>';
				}
				else 
				{
					echo '<option value="Information Technology">IT</option>';					
				}
				
				if($dept=="Master of Computer Application")
				{
					echo '<option value="Master of Computer Application" selected>MCA</option>';
				}
				else
				{
					echo '<option value="Master of Computer Application">MCA</option>';
				}
				
				if($dept=="Electrical Engineering")
				{
					echo '<option value="Electrical Engineering" selected>Electrical</option>';
				}
				else
				{
					echo '<option value="Electrical Engineering">Electrical</option>';
				}
				
				if($dept=="Civil Engineering")
				{
					echo '<option value="Civil Engineering" selected>Civil</option>';
				}
				else
				{
					echo '<option value="Civil Engineering">Civil</option>';
				}
				
				if($dept=="Mechanical Engineering")
				{
					echo '<option value="Mechanical Engineering" selected>Mechanical</option>';
				}
				else
				{
					echo '<option value="Mechanical Engineering">Mechanical</option>';
				}
				
				if($dept=="Electronics and Telecommunication")
				{
					echo '<option value="Electronics and Telecommunication" selected>ENTC</option>';
				}
				else
				{
					echo '<option value="Electronics and Telecommunication">ENTC</option>';
				}
			?>
		  </select>
		</label>
	  </div>
	  
	  <div class="col-3">
		<label>
		  Registation Fees/Entry fees
		  <input placeholder="What is entry fee for Event?" id="entry_fee" name="entry_fees" tabindex="6" value="<?php echo $reg_fees;?>">
		</label>
	  </div>
	  <div class="col-3">
		<label>
		Contact Person Name
		  <input placeholder="To who should I contact for event details ?" id="cpname" name="cpname" tabindex="7" value="<?php echo $cpname;?>">
		</label>
	  </div>
	  <div class="col-3">
		<label>
		Contact Person Mobile
		  <input placeholder="What is mobile number of contact person?" id="cpmob" name="cpmob" tabindex="8" value="<?php echo $cpmob;?>">
		</label>
	  </div>
 	  <div class="col-1">
		<label>
		Description of Event<br>
		  <textarea class="w3-input w3-border" placeholder="Tell me about your event in short?" style="width:600px;height:100px;" id="descp" name="descp" tabindex="8"><?php echo $desc;?></textarea>
	      <p class="w3-container w3-padding-8 w3-orange">Note: Do not use & and ' symbols</p>
		</label>
	  </div>

	  
	  <div class="col-submit">
		<button class="w3-btn w3-blue" onclick="javascript:basic();">Save</button>
	  </div>
	  </form>
	</div>

	<div id="d2" style="display:none">
	<center>
		
		<div class="w3-container w3-black w3-padding-8 w3-center" style="width:300px;border-radius: 32px 32px 1px 1px;"><Strong><h4>Detailed Information</h4></strong></div>
	</center>
				<div class="w3-container">
					<div class="w3-half w3-container w3-padding-16 w3-border-right">
						<label>
							<h4 class="w3-center"><strong>Rounds/Phase</strong></h4>
						<div class="w3-container" class="w3-input">
						Enter your rounds in below text box as per below example<br>
						<!---<textarea style="width:100%;height:250px" class=" w3-input" disabled>
<b>Novice</b><br>
<b>Round 1:</b>Technical Aptitude Test<br>
<b>Round 2:</b>Rapid Question Answer<br>
<b>Round 3:</b>Bombshell Round<br>
								
<b>Expert</b><br>
<b>Round 1:</b>Technical Aptitude Test<br>
<b>Round 2:</b>Rapid Question Answer<br>
<b>Round 3:</b>Bombshell Round<br>

Symbol Meaning:
<br> : Add NewLine
<b>  : Make text Bold between <b> and </b> 
						</textarea>-->
						
						</div>
							<p class="w3-text-red">Note: Don't Copy and Paste Rules and Details.</p>
						<p><textarea style="width:100%;height:250px;" placeholder="Enter your rounds here as per above format?" id="round1" class="w3-border w3-input" name="round1" ><?php echo $round1;?></textarea></p>
						<div style="display:none;">
						<p><textarea style="width:80%;" placeholder="What is ROUND 2?" id="round2" class="w3-border" name="round2" ><?php echo $round2;?></textarea></p>
						<p><textarea style="width:80%;" placeholder="What is ROUND 3?" id="round3" class="w3-border" name="round3" ><?php echo $round3;?></textarea></p>
						<p><textarea style="width:80%;" placeholder="What is ROUND 4?" id="round4" class="w3-border" name="round4" ><?php echo $round4;?> </textarea></p>
						<p><textarea style="width:80%;" placeholder="What is ROUND 5?" id="round5" class="w3-border" name="round5" ><?php echo $round5;?></textarea></p>
						</div>
						</label>
					</div>
					
					<div class="w3-half w3-container w3-padding-16">
						<label>
	
						<h4 class="w3-center"><strong>Rules/Details</strong></h4>
						<div class="w3-container" class="w3-input">
						Enter your Rules/Details in below text box as per below example
						<p class="w3-text-red">Note: Don't repeat information in 'details' which is already submitted basic details.(Max.participants,Entry fee,..)</p>
						<!---<textarea style="width:100%;height:250px" class=" w3-input" disabled>
1. No Negative Marking in aptitude test. <br>
2. Use of any form of electronic media to improve your score will be considered as an attempt to cheat. They will be Disqualified on the spot. <br>
3. Required Output should be in desired format.<br>

Symbol Meaning:
<br> : Add NewLine
						</textarea>-->
						
						</div>
						<p><textarea style="width:100%;height:250px;" placeholder="Enter your rules/details here as per above format?" id="rule1" class="w3-border w3-input" name="rule1" ><?php echo $rule1;?></textarea></p>
						<div style="display:none;">
						<p><textarea style="width:80%;" placeholder="What is RULE 2?" id="rule2" class="w3-border" name="rule2" ><?php echo $rule2;?></textarea></p>
						<p><textarea style="width:80%;" placeholder="What is RULE 3?" id="rule3" class="w3-border" name="rule3" ><?php echo $rule3;?></textarea></p>
						<p><textarea style="width:80%;" placeholder="What is RULE 4?" id="rule4" class="w3-border" name="rule4" ><?php echo $rule4;?></textarea></p>
						<p><textarea style="width:80%;" placeholder="What is RULE 5?" id="rule5" class="w3-border" name="rule5" ><?php echo $rule5;?></textarea></p>
						</div>
						</label>
					</div>
			</div>
			
			<div class="w3-container">
				<div class="w3-half w3-container w3-padding-16">
				
					Maximum Participants Per Team<br><br>
					 <input class="w3-input w3-border" placeholder="How many students per team?" value="<?php echo $maxp;?>" type="number" id="max_team" name="max_team"/>
				
				</div>
				<div class="w3-half w3-container w3-padding-16">
					Note
					<p><textarea class="w3-input w3-border" style="width:100%;" placeholder="Do you have any note for paticipants?" id="note" name="note"><?php echo $note;?></textarea></p>
				</div>
			</div>
				  <div class="col-submit">
				  <center id="result2"></center>
					<button class="w3-btn w3-blue" id="save_btn" onclick="javascript:detailed();">Save</button>
				  </div>
	</div>
	
	<div id="d3" style="display:none;min-height:500px;">
		<center>
			<div class="w3-container w3-black w3-padding-8 w3-center" style="width:300px;border-radius: 32px 32px 1px 1px;"><Strong><h4>Logo & Poster</h4></strong></div>
		</center>
		<div class="w3-container">
			<div class="w3-card-2 w3-container w3-padding-16 w3-half">
					<center><h4><b>Upload Logo</b></h4>		
						<p>Logo Requirements:<br>150px*150px and size between 20-100kb</p>
						<input type="text" value="<?php echo $_SESSION['event'];?>" name="eid" id="eid" style="display:none;">
						<div id="pre">
						</div>
						<form name="form_id" id="form_id" action="javascript:void(0);" enctype="multipart/form-data" style="width:400px;">  
								<input type="file" style="visibility: hidden;" name="vasplus_multiple_files" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
								<label for="file-1"  class="w3-btn w3-round" style="background-color:#2957a4; margin-top:30px;margin-bottom:20px;"><svg xmlns="http://www.w3.org/2000/svg" width="" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span class="w3-text-white " style="font-size:15px;">Choose a file</span></label><br>
								<br>
								<table class="w3-table w3-border w3-stripped" style="max-width:400px;font-size:15px;" id="add_files">
								<tr style="" >
								<tr>
								</table>
								<br><br>
								<input type="submit" value="&nbsp;&nbsp;Upload&nbsp;&nbsp;"  class="w3-btn w3-round w3-black" style="font-size:15px;margin-top:10px;margin-left:20px;margin-bottom:20px;" />
						</form>							
					</center>
			</div>
			<div class="w3-card-2 w3-container w3-padding-16 w3-half">
					<center><h4><b>Upload Banner</b></h4>		
						<p>Banner Requirements:<br>1300px*800px and size less than 3mb</p>
							<div id='preview'>
							</div>

							<form id="imageform" method="post" enctype="multipart/form-data" action='ajaximage.php'>
								<input type="file" style="visibility: hidden;" name="photoimg" id="photoimg"/>
								<label for="photoimg"  class="w3-btn w3-round" style="background-color:#2957a4; margin-top:30px;margin-bottom:20px;"><svg xmlns="http://www.w3.org/2000/svg" width="" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span class="w3-text-white " style="font-size:15px;">Choose a file</span></label>
							<!--<input type="file" name="photoimg" id="photoimg" style="font-size:15px;"/>-->
							</form>
							<br><p>Note: Once image is selected it will get uploded</p>
						</center>
			</div>
		</div>
		<center>
			<div class="w3-half w3-center"  style="margin-left:25%"> 
				<p class="w3-orange w3-padding-16">To resize,compress,crop image <a href="http://resizeimage.net/"><b>CLICK HERE.</b></a></p>
			</div>
		</center>
	</div>
	
	<div id="d4" style="min-height:500px;display:none;">
		<center>
			<div class="w3-container w3-black w3-padding-8 w3-center" style="width:300px;border-radius: 32px 32px 1px 1px;"><Strong><h4>The Team</h4></strong></div>
		</center>
		<div class="w3-container w3-border">
			<h6 style="margin-left:30px;">Select Role: 
													<select name="team_role" id="role" class="w3-input w3-border " onChange="javascript:get_member();" style="width:300px;">
														<option value="Event Head">Event Head</option>
														<option value="Event Co-Head">Event Co-Head</option>
														<option value="Department Head">Departmental Head</option>
														<option value="Assistant Department Head">Assistant Departmental Head</option>
														<option value="Treasurer">Treasurer</option>
													</select>
			</h6>
			<div id="member">
			<form onsubmit="return false" style="margin-top:-1px;">
			  <div class="col-2">
				<label>
				  First Name
				  <input placeholder="What is first name?" id="fname" name="fname">
				</label>
			  </div>
			  <div class="col-2">
				<label>
				  Last Name
				  <input placeholder="What is last name?" id="lname" name="lname">
				</label>
			  </div>
			  <div class="col-4">
				<label>
				  Mobile Number
				  <input placeholder="+919623299399" id="mobile_no" name="mobile_no">
				</label>
			  </div>
			  <div class="col-4">
				<label>
				  Email ID
				  <input placeholder="abc@xyz.com" id="email" name="email">
				</label>
			  </div>
			  <div class="col-4">
				<label>
				  Facebook Link
				  <input placeholder="http://facebook.com/surajsnavare" id="facebook" name="facebook">
				</label>
			  </div>
			  <div class="col-4">
				<label>
				  Linkedin Link
				  <input placeholder="http://linkedin.com/in/surajsnavare" id="linkedin" name="linkedin">
				</label>
			  </div>
			<div>
				<center>
						<button class="w3-btn w3-teal" onclick="javascript:add_member();" style="width:200px;margin-top:30px;">Add Member
						</button>
				</center>
		 </div>
			</form>
		</div>
		</div>
	</div>
	
	<div id="d5">
		<div class="w3-container">
			<h2><strong>Welcome <?php echo $name;?>,</strong></h2>
			<h5>
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This is a portal to manage your <b>event's page</b> on <b>Aavishkar website</b> (<a href="http://aavishkargcek.com">www.aavishkargcek.com</a>).Before starting please read and follow instructions below</p>
			<center>
				<p>
				<h4 class="w3-red ">
					<p class="blink_me"><a href="team" style="text-decoration:none">Portal is opened again for 2 days,  24 and 25 Feb 2018</a></p>
				</h4>

				<h4 class="w3-orange">
					Please don't mention prize amount anywhere on your webpage.
				</h4>

				</p>
			</center>
			<ol>
			<dl>
				<li>
					<dt><b>Side Navigation:</b></dt>
					<dd>Use side navigation to jump on different sections any time. </dd>
				</li>
				
				<li>
					<dt><b>Save Option:</b></dt>
					<dd>You must <u>save details</u> before moving to 'logo & banner' section else your details will not be saved.</dd>
				</li>

				<li>
					<dt><b>Detailed Information Section:</b></dt>
					<dd>Enter information as per instructions. It's for your convinince. You can change style of rules/details and round using HTML tags.It's upto you but if you don't have knowlede of html just follow given instructions.</dd>
				</li>

				<li>
					<dt><b>The team section:</b></dt>
					<dd>To add members select the role first then enter all details and click on 'Add Member'.</dd>
				</li>
				<li>
					<dt><b>Modifing or Updating Details:</b></dt>
					<dd>Just submit details again to update it. Details will get overwritten.</dd>
				</li>
				<li>
					<dt><b>Logo and Banner:</b></dt>
					<dd>Submit logo and banner in specified resolution for better look of your webpage.</dd>
				</li>
<!--				<p class="w3-orange w3-container"><b class="w3-text-red">Note: </b>Contents will get overwritten if details submitted twice.</p>-->
				<p class="w3-orange w3-container"><b class="w3-text-red">Note: </b>Use CTR+F5 to refresh page after uploading/Updating logo and banner to see the effect.</p>
			</ol>
			</dl>
			</h5>
		</div>
	</div>
	
</div>
<div style="background-color:#222a32;margin-left:17%;margin-top:4%;margin-bottom:0%;" class="w3-container w3-padding-16 w3-text-white">
		<center>
		Design and Developed by Suraj Nanavare
		</center>
</div>
<script type="text/javascript">
	
	function basic()
	{
			var ename=document.getElementById("ename").value;
			var tag=document.getElementById("tagline").value;
			var nontech=document.getElementById("non_technical").value;
			var day=document.getElementById("day").value;
			var dept=document.getElementById("department").value;
			var entry=document.getElementById("entry_fee").value;
			var cpname=document.getElementById("cpname").value;
			var cpmob=document.getElementById("cpmob").value;
			var desc=document.getElementById("descp").value;
			var  hr=new XMLHttpRequest();
			var url="add_basic_details.php";
			hr.open("POST",url,true);
			var vars="ename="+ename+"&tag="+tag+"&nontech="+nontech+"&day="+day+"&dept="+dept+"&entry="+entry+"&cpname="+cpname+"&cpmob="+cpmob+"&desc="+desc;
			
			hr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			hr.onreadystatechange=function(){
				if(hr.readyState==4 && hr.status==200)
				{
					var rdata=hr.responseText;
					if(rdata=='404')
					{
						document.getElementById("result3").innerHTML='<p style="color:red;">Technical Error!</p>';				
					}
					else
					{
					   document.getElementById("result3").innerHTML=rdata;
					}
					
				}
				
			}
			hr.send(vars);
			document.getElementById("result3").innerHTML='<div class="w3-panel w3-yellow w3-round  w3-animate-right" style="min-width: 400px;margin-left: 10px;height: 10%;position: fixed;top: 85%;right:1% ;"><b><p><li class="fa fa-spinner"></li> Please Wait</p></b></div>';
	}
	
	
	
	
	function _detailed()
	{
			var round1=document.getElementById("round1").value;
			var round2=document.getElementById("round2").value;
			var round3=document.getElementById("round3").value;
			var round4=document.getElementById("round4").value;
			var round5=document.getElementById("round5").value;
			var rule1=document.getElementById("rule1").value;
			var rule2=document.getElementById("rule2").value;
			var rule3=document.getElementById("rule3").value;
			var rule4=document.getElementById("rule4").value;
			var rule5=document.getElementById("rule5").value;
			var max_team=document.getElementById("max_team").value;
			var note=document.getElementById("note").value;
			var  hr=new XMLHttpRequest();
			var url="add_detailed_info.php";
			hr.open("POST",url,true);
			var vars="round1="+round1+"&round2="+round2+"&round3="+round3+"&round4="+round4+"&round5="+round5+"&rule1="+rule1+"&rule2="+rule2+"&rule3="+rule3+"&rule4="+rule4+"&rule5="+rule5+"&max_team="+max_team+"&note="+note;
			
			hr.setRequestHeader("Content-type","text");
			hr.onreadystatechange=function(){
				if(hr.readyState==4 && hr.status==200)
				{
					var rdata=hr.responseText;
					if(rdata=='404')
					{
						document.getElementById("result3").innerHTML='<p style="color:red;">Technical Error!</p>';				
					}
					else
					{
					   document.getElementById("result3").innerHTML=rdata;
					}
					
				}
				
			}
			hr.send(vars);
			document.getElementById("result3").innerHTML='<div class="w3-panel w3-yellow w3-round  w3-animate-right" style="min-width: 400px;margin-left: 10px;height: 10%;position: fixed;top: 85%;right:1% ;"><b><p><li class="fa fa-spinner"></li> Please Wait</p></b></div>';

	}
	function add_member()
	{
			var role=document.getElementById("role").value;
			var fname=document.getElementById("fname").value;
			var lname=document.getElementById("lname").value;
			var mob=document.getElementById("mobile_no").value;
			var email=document.getElementById("email").value;
			var flink=document.getElementById("facebook").value;
			var llink=document.getElementById("linkedin").value;
			
			var  hr=new XMLHttpRequest();
			var url="add_team.php";
			hr.open("POST",url,true);
			var vars="role="+role+"&fname="+fname+"&lname="+lname+"&mob="+mob+"&email="+email+"&flink="+flink+"&llink="+llink;
			
			hr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			hr.onreadystatechange=function(){
				if(hr.readyState==4 && hr.status==200)
				{
					var rdata=hr.responseText;
					if(rdata=='404')
					{
						document.getElementById("result3").innerHTML='<p style="color:red;">Technical Error!</p>';				
					}
					else
					{
					   document.getElementById("result3").innerHTML=rdata;
					}
					
				}
				
			}
			hr.send(vars);
			document.getElementById("result3").innerHTML='<div class="w3-panel w3-yellow w3-round  w3-animate-right" style="min-width: 400px;margin-left: 10px;height: 10%;position: fixed;top: 85%;right:1% ;"><b><p><li class="fa fa-spinner"></li> Please Wait</p></b></div>';

	}
	function get_member()
	{
			var role=document.getElementById("role").value;
			var  hr=new XMLHttpRequest();
			var url="get_member.php";
			hr.open("POST",url,true);
			var vars="role="+role;
			hr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			hr.onreadystatechange=function(){
				if(hr.readyState==4 && hr.status==200)
				{
					var rdata=hr.responseText;
					if(rdata=='404')
					{
						document.getElementById("result3").innerHTML='<p style="color:red;">Technical Error!</p>';				
					}
					else
					{
					   document.getElementById("member").innerHTML=rdata;
					   document.getElementById("result3").innerHTML='';
					}
					
				}
				
			}
			hr.send(vars);
			document.getElementById("result3").innerHTML='<div class="w3-panel w3-yellow w3-round  w3-animate-right" style="min-width: 400px;margin-left: 10px;height: 10%;position: fixed;top: 85%;right:1% ;"><b><p><li class="fa fa-spinner"></li> Please Wait</p></b></div>';

	}



	

<?php
}
else
{
	echo "Requested page not found!Please login to access this page!";
}
?>
</script>
<div id="result3">

</div>
</body>
</html>