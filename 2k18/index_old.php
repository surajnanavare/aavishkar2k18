<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Aavishkar2k18 | Events</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link href="https://fonts.googleapis.com/css?family=Laila" rel="stylesheet">
  <link rel="stylesheet" href="css/font-awesome.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  
      <style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      body {
  font-family: "Open Sans", sans-serif;
  height: 100vh;
  background: url("https://i.imgur.com/HgflTDf.jpg") 50% fixed;
  background-size: cover;
}

@keyframes spinner {
  0% {
    transform: rotateZ(0deg);
  }
  100% {
    transform: rotateZ(359deg);
  }
}
* {
  box-sizing: border-box;
}

.wrapper {
  display: flex;
//  align-items: right;
  flex-direction: column;
  justify-content: center;
  width: 100%;
  min-height: 100%;
  padding: 20px;
  background: rgba(4, 40, 68, 0.85);
}

.login {
  border-radius: 2px 2px 5px 5px;
  padding: 10px 20px 20px 20px;
  width: 90%;
  max-width: 320px;
  background: #ffffff;
  position: relative;
  padding-bottom: 80px;
  box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.3);
}
.login.loading button {
  max-height: 100%;
  padding-top: 50px;
}
.login.loading button .spinner {
  opacity: 1;
  top: 40%;
}
.login.ok button {
  background-color: #8bc34a;
}
.login.ok button .spinner {
  border-radius: 0;
  border-top-color: transparent;
  border-right-color: transparent;
  height: 20px;
  animation: none;
  transform: rotateZ(-45deg);
  
}
.login input {
  display: block;
  padding: 15px 10px;
  margin-bottom: 10px;
  width: 100%;
  border: 1px solid #ddd;
  transition: border-width 0.2s ease;
  border-radius: 2px;
  color: #ccc;
}
.login input + i.fa {
  color: #fff;
  font-size: 1em;
  position: absolute;
  margin-top: -47px;
  opacity: 0;
  left: 0;
  transition: all 0.1s ease-in;
}
.login input:focus {
  outline: none;
  color: #444;
  border-color: #2196F3;
  border-left-width: 35px;
}
.login input:focus + i.fa {
  opacity: 1;
  left: 30px;
  transition: all 0.25s ease-out;
}
.login a {
  font-size: 0.8em;
  color: #2196F3;
  text-decoration: none;
}
.login .title {
  color: #444;
  font-size: 1.2em;
  font-weight: bold;
  margin: 10px 0 30px 0;
  border-bottom: 1px solid #eee;
  padding-bottom: 20px;
}
.login button {
  width: 100%;
  height: 100%;
  padding: 10px 10px;
  background: #2196F3;
  color: #fff;
  display: block;
  border: none;
  margin-top: 20px;
  position: absolute;
  left: 0;
  bottom: 0;
  max-height: 60px;
  border: 0px solid rgba(0, 0, 0, 0.1);
  border-radius: 0 0 2px 2px;
  transform: rotateZ(0deg);
  transition: all 0.1s ease-out;
  border-bottom-width: 7px;
}

.login:not(.loading) button:hover {
  box-shadow: 0px 1px 3px #2196F3;
}
.login:not(.loading) button:focus {
  border-bottom-width: 4px;
}

footer {
  display: block;
  padding-top: 50px;
  text-align: center;
  color: #ddd;
  font-weight: normal;
  text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.2);
  font-size: 0.8em;
}
footer a, footer a:link {
  color: #fff;
  text-decoration: none;
}
.w3-container:after,.w3-panel:after,.w3-row:after,.w3-row-padding:after,.w3-topnav:after,.w3-clear:after,.w3-btn-group:before,.w3-btn-group:after,.w3-btn-bar:before,.w3-btn-bar:after
{content:"";display:table;clear:both}
.w3-col,.w3-half,.w3-third,.w3-twothird,.w3-threequarter,.w3-quarter{float:left;width:100%}
.w3-col.s1{width:8.33333%}
.w3-col.s2{width:16.66666%}
.w3-col.s3{width:24.99999%}
.w3-col.s4{width:33.33333%}
.w3-col.s5{width:41.66666%}
.w3-col.s6{width:49.99999%}
.w3-col.s7{width:58.33333%}
.w3-col.s8{width:66.66666%}
.w3-col.s9{width:74.99999%}
.w3-col.s10{width:83.33333%}
.w3-col.s11{width:91.66666%}
.w3-col.s12,.w3-half,.w3-third,.w3-twothird,.w3-threequarter,.w3-quarter{width:99.99999%}
@media only screen and (min-width:601px){
.w3-col.m1{width:8.33333%}
.w3-col.m2{width:16.66666%}
.w3-col.m3,.w3-quarter{width:24.99999%}
.w3-col.m4,.w3-third{width:33.33333%}
.w3-col.m5{width:41.66666%}
.w3-col.m6,.w3-half{width:49.99999%}
.w3-col.m7{width:58.33333%}
.w3-col.m8,.w3-twothird{width:66.66666%}
.w3-col.m9,.w3-threequarter{width:74.99999%}
.w3-col.m10{width:83.33333%}
.w3-col.m11{width:91.66666%}
.w3-col.m12{width:99.99999%}}
@media only screen and (min-width:993px){
.w3-col.l1{width:8.33333%}
.w3-col.l2{width:16.66666%}
.w3-col.l3,.w3-quarter{width:24.99999%}
.w3-col.l4,.w3-third{width:33.33333%}
.w3-col.l5{width:41.66666%}
.w3-col.l6,.w3-half{width:49.99999%}
.w3-col.l7{width:58.33333%}
.w3-col.l8,.w3-twothird{width:66.66666%}
.w3-col.l9,.w3-threequarter{width:74.99999%}
.w3-col.l10{width:83.33333%}
.w3-col.l11{width:91.66666%}
.w3-col.l12{width:99.99999%}}

    </style>


</head>

<body>
<?php
session_start();
if(isset($_SESSION['event']))
{
	echo '<meta http-equiv="refresh" content="0;url=home.php">';
}
?>
	  <div class="wrapper" style="margin-right:10px;">
		<div class="w3-row">
			<div class="w3-col l7">
				<div class="w3-container">
				<center>
					<p style="font-family:'laila',serif;font-size:60px;color:white;margin-left:5%"><strong>Aavishkar2k18</strong></p>
					<p><h2 style="width:70%;color:white;font-family:'laila',serif;line-height:35px;">Welcome to event management portal! Add or Modify your event details on one click.</h2></p>
				<center>
				</div>
			</div>
  
		   <div class="w3-col l5">
		   <center>
			    <div class="login" id="data">
				<center><h2><p class="title" style="font-family:'laila',serif;">Login</p></h2><p id="result"></p></center>
				<input type="text" name="username" id="username" placeholder="Username" autofocus/>
				<i class="fa fa-user"></i>
				<input type="password" name="password" id="password" placeholder="Password" />
				<i class="fa fa-key"></i>
				<p onclick="javascript:forget_pass();" style="color:blue;cursor:pointer;"> Forgot your password?</p>
				<button onclick="javascript:login();" style="cursor:pointer;">
				  <span class="state" >Log in</span>
				</button>
			  </div>
			</center>
		   </div>
		</div>
	</div>
    <script  src="js/index.js"></script>
<script>
	function login()
	{
			/*alert("Login is closed for while!");*/
			var username=document.getElementById("username").value;
			var password=document.getElementById("password").value;
			var  hr=new XMLHttpRequest();
			var url="login.php";
			hr.open("POST",url,true);
			var vars="user="+username+"&pass="+password;
			
			hr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			hr.onreadystatechange=function(){
				if(hr.readyState==4 && hr.status==200)
				{
					var rdata=hr.responseText;
					if(rdata=='404')
					{
						document.getElementById("result").innerHTML='<p style="color:red;">Technical Error!</p>';				
					}
					else
					{
					   document.getElementById("result").innerHTML=rdata;
					}
					
				}
				
			}
			hr.send(vars);
			document.getElementById("result").innerHTML="<p><li class='fa fa-spinner'></li> Please Wait....</p>";
	}
	function forget_pass()
	{
		document.getElementById("result").innerHTML="<p>Please contact on surajnanavare@aavishkargcek.com or 9623299399</p>";
	}
</script>
<!-- Start of SimpleHitCounter Code -->
<div align="center" style="display:none;"><a href="http://www.simplehitcounter.com" target="_blank"><img src="http://simplehitcounter.com/hit.php?uid=2309022&f=16777215&b=0" border="0" height="18" width="83" alt="web counter"></a><br></div>
<!-- End of SimpleHitCounter Code -->

</body>
</html>
