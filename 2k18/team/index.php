<?php
session_start();
include 'connect.php';
$eid = $_GET['eid'];
?>
<html>
<head>
	<title>Team Members | Pictures</title>
	<link href="http://aavishkargcek.com/content/themes/mytheme/mytheme/styled3b3.css?ver=v1" rel="stylesheet">
	<link href="http://aavishkargcek.com/content/themes/mytheme/mytheme/css/bootstrapd7b7.css?ver=4.3" rel="stylesheet">
	<link href="http://aavishkargcek.com/content/themes/mytheme/mytheme/css/bootstrapd7b7.css?ver=4.3" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="https://fonts.googleapis.com/css?family=Laila" rel="stylesheet">
	 <!-- Css files-->
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/style-example.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.Jcrop.css" />

    <!-- Js files-->
    <script type="text/javascript" src="scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="scripts/jquery.Jcrop.js"></script>
    <script type="text/javascript" src="scripts/jquery.SimpleCropper.js"></script>
	
	
<script>
window.onload = function(){
	  $.ajax({
        url: "",
        context: document.body,
        success: function(s,x){
		
            $('html[manifest=saveappoffline.appcache]').attr('content', '');
                $(this).html(s);
        }
    }); 
	
}

function select2(obj) {
		document.getElementById("upload").value="Upload"
        $('#file').click();
}

function showImg(){
	
	if(this.files && this.files[0])
	{
		var obj= new FileReader();
		obj.onload = function(data){
			var image = document.getElementById("image1");
			image.src = data.target.result;
		}
		obj.readAsDataURL(this.files[0]);
	}
}

</script>
</head>
<body>
<?
if($_SESSION['event']!="")
{	

?>
<div class="w3-top" style="z-index:999;">
	  <div class="w3-bar w3-text-white w3-card-2" style="background-color:#222a32;">
		<a href="" class="w3-bar-item w3-button" style="font-family: 'Laila', serif; text-decoration:none;"><b style="font-size:25px;" class="w3-text-white">Aavishkar2k18 | Events</b></a>		
		<div  class="w3-right w3-green w3-container w3-padding-16" style="cursor:pointer" onclick="document.getElementById('id01').style.display='block'">Need Help</div><a href="http://aavishkargcek.com/2k18/" style="width:90px;margin-top:6px;margin-right:10px; text-decoration:none;" class="w3-btn w3-white w3-right ">Home</a>
	  </div>
</div>
<div style="margin-top:60px;">
	<center>
		<div class="w3-container">
		<div class="w3-red w3-padding-16 w3-half"><h5><strong>Upload Picture of your Teammates Here!</strong></h5></div>
		<div class="w3-half w3-orange w3-padding-16"><h5><strong>Select Member <img src="arrow.png" width="14px"> Select Picture <img src="arrow.png" width="14px"> Uplod</strong></h5></div>
		<span class="w3-padding-4 w3-container " style="cursor:pointer" onclick="document.getElementById('id02').style.display='block'"><strong>Click here to Crop Image.</strong></span>
		</div>
	</center>
<div><br>
	<div class="w3-row">
		<div class="w3-col l3 w3-card w3-animate-left" data-scrollreveal="enter left after 0.2s over 1s" data-sr-init="true" data-sr-complete="true">
	
			<form enctype="multipart/form-data" action="" method="post">
			<br>
			<div class="team-member" style="20px;">
				<figure class="profile-pic" style="border: 1px solid black;">
				<?php 
						echo '<img id ="image1" src="no1.jpg" alt="Upload Picture">';
				?>
					
				</figure>
				<div class="member-details">
					<div id="info" style="margin-bottom:20px;">
					</div>
						<select name="role" id="role" onchange="javascript:get_member();" class="w3-input w3-border" style="font-family:initial;">
							<option value="Default">Select Member</option>
							<option value="Event Head">Event Head</option>
							<option value="Event Co-Head">Event Co-Head</option>
							<option value="Department Head">Departmental Head</option>
							<option value="Assistant Department Head">Assistant Departmental Head</option>
							<option value="Treasurer">Treasurer</option>		
						</select>

						<div id="filediv"><input name="file[]" type="file" onchange="showImg.call(this)" id="file" style="display:none;"></div><br/>           
						<input type="button" id="select" class="upload" name="#file" value="Select Pic" style="background-color:#516dcc;padding: 13px 25px 13px 25px;" onclick="javascript:select2(this);"/>
						<input type="submit" value="Upload" name="submit" id="upload" style="padding: 13px 25px 13px 25px;" class="upload"/>
			</form>
				</div>
			</div>
		</div>
		
		<div class="w3-col l9" >
			<div class="w3-row w3-card " style="padding-top:30px;">
				<h2 class="dark-text">THE TEAM</h2>
				<h6>Meet the people that made it all happen.</h6><br>
		
			<?php	
				include "upload.php";
				$sql  = "SELECT * FROM team WHERE eid='".$_SESSION['event']."'";
				$result = $conn->query($sql);
				if ($result->num_rows > 0) {
					while($row = $result->fetch_assoc()) {
						$fname=$row['fname'];
						$lname=$row['lname'];
						$role=$row['role'];		
						$pic= "";
						if(!empty($fname) ||!empty($lname)) 
						{
							if(file_exists("pics/".strtoupper($fname)."_".strtoupper($lname).".jpg"))
							{
								$pic = "pics/".strtoupper($fname)."_".strtoupper($lname).".jpg";
								
							}else{
								$pic = "no1.jpg";
							}							
							echo '
								<div class="w3-col l3">
									<div class="team-member" style="20px;">
										<figure class="profile-pic" style="border: 1px solid black;">
											<img src="'.$pic.'">									
										</figure>
										<h6 class="dark-text red-border-bottom" id="dname">'.strtoupper($fname).' '.strtoupper($lname).'<br>('.$role.')</h6>
									</div>
								 </div>';
						}
					}
				}
			?>
			</div>
		</div>
	</div>
	
	<div id="id01" class="w3-modal">
		<div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:700px">
			<div class="w3-container w3-padding-16 ">
					<h3 class="w3-left"><strong>How It Work's? Let's have a look..</strong></h3>
					<hr><br>
					<h5>
						<ul>
							<li class="w3-left">First Select Team Member Role. It will show member info.</li>
							<li class="w3-left">Then click on <input type="button" id="select" value="Select" style="background-color:#516dcc;padding: 13px 25px 13px 25px;"/> to select picture.<p class="w3-text-red">(Note: Picture should be in 172px * 172 px.)</p></li>
							<li class="w3-left">Now finally Click on <input type="submit" value="Upload" style="padding: 13px 25px 13px 25px;" class="upload"/>You will see picture on page. <p class="w3-text-red">(Note: If needed refresh page : CTR + F5).</p></li>
							<li class="w3-left"><b>Short Cut:</b> Use Up and Down keys to change Member.</li>
							
						</ul>
					</h5>
			</div>
			<div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
			   <button onclick="document.getElementById('id01').style.display='none'" type="button" class="w3-button w3-red w3-right">Close</button>
			</div>
		</div>
	</div>
	<div id="id02" class="w3-modal">
		<div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:700px">
			<div class="w3-container w3-padding-16 ">
					<h3 class="w3-left"><strong>Crop your image here!</strong></h3>
					<hr><br>
					<div class="w3-row">
					    <div class="simple-cropper-images w3-col l6">
							<div class="cropme" style="width: 300px; height: 300px;"></div>
							<div class="clear"></div>
							<script>
								// Init Simple Cropper
								$('.cropme').simpleCropper();
							</script>
						</div>
						<div class="w3-half w3-container w3-padding-4 w3-col l6 w3-text-black">
							<h4 class="w3-text-black w3-left" style="font-family:initial"><strong>How To Do IT?</strong></h4><br><br>
							<p class=" w3-text-black"><b>Step 1:</b> Click on Camera icon & Select Picture</p>
							<p class=" w3-text-black"><b>Step 2:</b> Crop Picture</p>
							<p class=" w3-text-black"><b>Step 3:</b> Right Click on New Image and Click on 'Save as'.</p>
							<p class=" w3-text-black"><b>Step 4:</b> Now upload New Picture on your portal.</p>
						
						</div>
					</div>
			</div>
			<div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
			   <button onclick="document.getElementById('id02').style.display='none'" type="button" class="w3-button w3-red w3-right">Close</button>
			</div>
		</div>
	</div>

	<script>
	function get_member()
	{
			var role=document.getElementById("role").value;
			var  hr=new XMLHttpRequest();
			var url="get_member.php";
			hr.open("POST",url,true);
			var vars="role="+role;
			hr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			hr.onreadystatechange=function(){
				if(hr.readyState==4 && hr.status==200)
				{
					var rdata=hr.responseText;
					if(rdata=='404')
					{
						document.getElementById("dname").innerHTML='<p style="color:red;">Technical Error!</p>';				
					}
					else
					{
					   document.getElementById("info").innerHTML=rdata;
					}
					
				}
				
			}
			hr.send(vars);
			document.getElementById("info").innerHTML='Please Wait...';

	}
	var option=0;
	document.onkeydown = function(e) {
    switch (e.keyCode) {
            case 38:
				option = (option +1)%6;
                document.getElementById("role").selectedIndex = option;   
				$("#role").trigger("change");			
				break;

			case 40:
				if(option<6 && option >0)
				{
					option = (option-1)%6;
					document.getElementById("role").selectedIndex = option;         
					$("#role").trigger("change");			
				}
				break;
    }
};
	</script>
</div>

<?php
}
else
{
	echo "Requested page is not available!";
}
?>
</body>
</html>