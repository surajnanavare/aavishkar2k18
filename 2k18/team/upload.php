<?php
$name = $_POST['name'];
if (isset($_POST['submit'])) {
	
    $j = 0; //Variable for indexing uploaded image 
	$target_path = "pics/"; //Declaring Path for uploaded images
    for ($i = 0; $i < count($_FILES['file']['name']); $i++) {//loop to get individual element from the array
       
		$validextensions = array("jpeg", "jpg");  //Extensions which are allowed
        $ext = explode('.', basename($_FILES['file']['name'][$i]));//explode file name from dot(.) 
        $file_extension = end($ext); //store extensions in the variable
        
		$target_path = $target_path . $name . "." . $ext[count($ext) - 1];//set the target path with a new name of image
        $j = $j + 1;//increment the number of uploaded images according to the files in array       
   	 if(empty($name))
	 {
		echo '<script>alert("Please Select Team Member!");</script>';
	 }
	 else if( basename($_FILES['file']['name'][$i])=="")
	  {
		  echo '<script>alert("Please Select File!");</script>';
	  }
	  else if (($_FILES["file"]["size"][$i] < 100000*6) //Approx. 100kb files can be uploaded.
                && in_array($file_extension, $validextensions)) {
            if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $target_path)) {//if file moved to uploads folder
					echo '<script>
						  document.getElementById("upload").value="Done";
						  document.getElementById("upload").style.backgroundColor="#4caf50";
						  </script>';
            } else {//if file was not moved.
                echo '<script>alert("Error while uploading file!");</script>';
            }
        } else {//if file size and file type was incorrect.
            echo '<script>alert("Invalid file size or type!Only .jpg or .jpge files are allowed. File size should be less than 600KB.");</script>';
        }
    }
}


?>