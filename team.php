<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>AAVISHKAR 2k18 | A National Level Technical Symposium</title>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}

.spinner {
  width: 40px;
  height: 40px;

  position: relative;
  margin: 250px auto;
}

.double-bounce1, .double-bounce2 {
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background-color: #abb2b9;
  opacity: 0.6;
  position: absolute;
  top: 0;
  left: 0;
  
  -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
  animation: sk-bounce 2.0s infinite ease-in-out;
}

.double-bounce2 {
  -webkit-animation-delay: -1.0s;
  animation-delay: -1.0s;
}

@-webkit-keyframes sk-bounce {
  0%, 100% { -webkit-transform: scale(0.0) }
  50% { -webkit-transform: scale(1.0) }
}

@keyframes sk-bounce {
  0%, 100% { 
    transform: scale(0.0);
    -webkit-transform: scale(0.0);
  } 50% { 
    transform: scale(1.0);
    -webkit-transform: scale(1.0);
  }
}

</style>
<?php
$host="199.79.62.23:3306";
$user="aavishka";
$pass="events_aavishkar@17";
$db="aavishka_events";
$conn = new mysqli($host, $user, $pass, $db);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
<link rel="stylesheet" type="text/css" href="content/themes/mytheme/mytheme/css/component.css" />
<link rel="stylesheet" type="text/css" href="content/themes/mytheme/mytheme/css/w3.css" />
<link rel='stylesheet' id='zerif_bootstrap_style-css'  href='content/themes/mytheme/mytheme/css/bootstrapd7b7.css?ver=4.3' type='text/css' media='all' />
<link rel='stylesheet' id='zerif_fontawesome-css'  href='content/themes/mytheme/mytheme/css/font-awesome.mind3b3.css?ver=v1' type='text/css' media='all' />
<link rel='stylesheet' id='zerif_pixeden_style-css'  href='content/themes/mytheme/mytheme/css/pixeden-iconsd3b3.css?ver=v1' type='text/css' media='all' />
<link rel='stylesheet' id='zerif_style-css'  href='content/themes/mytheme/mytheme/styled3b3.css?ver=v1' type='text/css' media='all' />
<script type='text/javascript' src='includes/js/jquery/jqueryc1d8.js?ver=1.11.3'></script>
<script type='text/javascript' src='includes/js/jquery/jquery-migrate.min1576.js?ver=1.2.1'></script>
<meta name="generator" content="AAVISHKAR" />
<link rel="icon" href="content/uploads/2015/09/cropped-index-32x32.png" sizes="32x32" />
</head>	

<body class="home blog custom-background" >


<div class="preloader" style="background-color:black">
	<div>
			
			<div class="spinner">
			  <div class="double-bounce1"></div>
			  <div class="double-bounce2"></div>
			</div>
			
	</div>
</div>



<header id="home" class="header">

<div id="main-nav" class="navbar navbar-inverse bs-docs-nav w3-card-2" role="banner">
		<div class="container">
			<div class="navbar-header responsive-logo w3-left">
				<a href="index.html" class="navbar-brand"><img src="content/themes/mytheme/mytheme/images/logo.png" alt="AAVISHKAR"></a>
				<div style="top:15px;margin-left:65px; position:absolute">
					<a href="index.html" class="navbar-brand" style="margin-top: -6px;"><img src="content/themes/mytheme/mytheme/images/logo2.png" style="margin-top:-10px;" width="90px" alt="AAVISHKAR"></a>
				</div>
			</div>
			<div class="w3-right">
				<a href="http://aavishkargcek.com/home.html" class="w3-btn w3-black w3-round" style="margin-top:20px;">Home</a>
			</div>
		</div>
</div>
</header> 


<div id="content" class="site-content">
<section class="our-team" id="team" style="padding-top:10px;background-color:black;">
<div class="container">
	<div class="section-header">
		<h2 class="dark-text" style="color:white;">THE TEAM</h2>
		<h6 class="dark-text">Meet the people that made it all happen.</h6>
	</div>

<div class="row">
			
	<?php
		$sql= "SELECT * from core_team order by sr_no asc ";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$name=$row['name'];
			$post=$row['post'];
			$mob=$row['mobile'];
			$email=$row['email'];
			$flink=$row['fb_link'];
			$llink=$row['linkedin_link'];
			if(!empty($name) && !empty($post))
			{						
					$pic= "";
					if(strpos($name, 'Miss. ') !== false)
						$name2=str_replace("Miss. ","",$name);
					else if(strpos($name, 'Mr. ') !== false)
						$name2=str_replace("Mr. ","",$name);
					
					$pic_name = str_replace(" ","_",$name2);
					if(file_exists("2k18/core_team/pics/".trim(strtoupper($pic_name),' ').".jpg"))
					{
						$pic = "2k18/core_team/pics/".trim(strtoupper($pic_name),' ').".jpg";
					}		
					else if(file_exists("2k18/core_team/pics/".trim(strtoupper($pic_name),' ').".jpg"))
					{
						$pic = "2k18/core_team/pics/".trim(strtoupper($pic_name),' ').".jpg";
						
					}else{
						if(strpos($name, 'Mr. ') !== false)
							$pic = "content/themes/mytheme/mytheme/images/no1.jpg";
						else if(strpos($name, 'Miss. ') !== false || strpos($name, 'Mrs. ') !== false)
							$pic = "2k18/core_team/pics/female.png";
						else 
							$pic = $pic = "content/themes/mytheme/mytheme/images/no1.jpg";
					}		
		?>
		<div class="col-lg-3 col-sm-3 team-box" data-scrollreveal="enter left after 0.2s over 1s">
			<div class="team-member">
				<figure class="profile-pic">
					<img src="<?php echo $pic;?>" alt="">
				</figure>
				<div class="member-details">
					<h5 class="dark-text red-border-bottom"><?php echo $name;?></h5>
					<div class="position"><?php echo $post;?></div>
				</div>
				<div class="social-icons">
					<ul>
							<li><a href="tel:+91<?php echo $mob;?>" title="+91<?php echo $mob;?>"><i class="fa fa-mobile-phone"></i></a></li>
							<li><a href="mailto:<?php echo $email;?>" title="<?php echo $email;?>"><i class="fa fa-envelope-o"></i></a></li>
							<?php
								 if(!empty($flink))
								 {
									 echo '<li><a href="'.$flink.'"><i class="fa fa-facebook"></i></a></li>';                        
								 }
								 else
								 {
									 echo '<li><a ><i class="fa fa-facebook"></i></a></li>';                        
								 }
									//echo '<li><a ><i class="fa fa-google-plus-square"></i></a></li>';
									
								 if(!empty($llink))
								 {
									echo '<li><a href="'.$llink.'"><i class="fa fa-linkedin"></i></a></li>';
								 }
								 else
								 {
									 echo '<li><a ><i class="fa fa-linkedin"></i></a></li>';
								 }
							?>
					</ul>
				</div>
			</div>
		</div>
	<?php
		}
	  }
	}
	?>

</div>
</div>
</section>


<footer id="footer">

<div class="container">

<div class="col-md-3 copyright">
<div class="zerif-copyright-box">
<a class="aavishkar-copyright" target="_blank" style="color:white;" rel="nofollow">Designed with Love & Care<br>TEAM AAVISHKAR 2k18</a>
</div>
</div>
</div>

</footer>
<script type='text/javascript' src='content/themes/mytheme/mytheme/js/bootstrap.min11a8.js?ver=20120206'></script>
<script type='text/javascript' src='content/themes/mytheme/mytheme/js/jquery.knob11a8.js?ver=20120206'></script>
<script type='text/javascript' src='content/themes/mytheme/mytheme/js/smoothscroll11a8.js?ver=20120206'></script>
<script type='text/javascript' src='content/themes/mytheme/mytheme/js/scrollReveal11a8.js?ver=20120206'></script>
<script type='text/javascript' src='content/themes/mytheme/mytheme/js/zerif11a8.js?ver=20120206'></script>

</body>
</html>
